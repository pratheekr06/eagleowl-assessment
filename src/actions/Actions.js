import * as actionTypes from './actionTypes';
import axios from 'axios';

const URL = 'https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/margin-group/';
const URL2 = 'https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/fluctuation-group/';
const URL3 = 'https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/recipes';

const actions = {
    getHighMargin,
    getLowMargin,
    getFluctuateMargin,
    getAllRecipe,
    getIncorrectRecipe,
    getDisabledRecipe,
    getUntaggedRecipe
}

function getHighMargin() {
    return async dispatch => {
        axios.get(`${URL}?order=top`).then (res => {
            dispatch({
                type: actionTypes.GET_HIGH_MARGIN,
                payload: res.data,
            })
        })
        .catch(err => {
            console.log(err);
        })
    }
}

function getLowMargin() {
    return async dispatch => {
        axios.get(`${URL}?order=bottom`).then (res => {
            dispatch({
                type: actionTypes.GET_LOW_MARGIN,
                payload: res.data,
            })
        })
        .catch(err => {
            console.log(err);
        })
    }
}

function getFluctuateMargin() {
    return async dispatch => {
        axios.get(`${URL2}?order=top`).then (res => {
            dispatch({
                type: actionTypes.GET_FLUCTUATE_MARGIN,
                payload: res.data,
            })
        })
        .catch(err => {
            console.log(err);
        })
    }
}

function getAllRecipe(page) {
    return async dispatch => {
        axios.get(`${URL3}?page=${page}`).then (res => {
            dispatch({
                type: actionTypes.GET_RECIPE,
                payload: res.data,
            })
        })
        .catch(err => {
            console.log(err);
        })
    }
}

function getIncorrectRecipe() {
    return async dispatch => {
        axios.get(`${URL3}?is_incorrect=true`).then (res => {
            dispatch({
                type: actionTypes.GET_INCORRECT_RECIPE,
                payload: res.data,
            })
        })
        .catch(err => {
            console.log(err);
        })
    }
}

function getDisabledRecipe() {
    return async dispatch => {
        axios.get(`${URL3}?is_disabled=true`).then (res => {
            dispatch({
                type: actionTypes.GET_DISABLED_RECIPE,
                payload: res.data,
            })
        })
        .catch(err => {
            console.log(err);
        })
    }
}

function getUntaggedRecipe() {
    return async dispatch => {
        axios.get(`${URL3}?is_untagged=true`).then (res => {
            dispatch({
                type: actionTypes.GET_UNTAGGED_RECIPE,
                payload: res.data,
            })
        })
        .catch(err => {
            console.log(err);
        })
    }
}

export default actions;