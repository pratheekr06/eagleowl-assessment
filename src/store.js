import { createStore, applyMiddleware, compose } from "redux";
import Reducer from "./reducers/Reducer";
import thunk from "redux-thunk";
import { logger } from "redux-logger";

const composeEnhancers =
  process.env.NODE_ENV === "development"
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : null || compose;

const store = createStore(
  Reducer,
  composeEnhancers(applyMiddleware(thunk, logger))
);

export default store;
