import * as actionTypes from "../actions/actionTypes";

const initialState = {
    highMargin: [],
    lowMargin: [],
    fluctuate: [],
    recipes: [],
    incorrectRecipe: [],
    disabledRecipe: [],
    untaggedRecipe: [],
}

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.GET_HIGH_MARGIN:
            return {
                ...state,
                highMargin: action.payload.results,
            };
            case actionTypes.GET_LOW_MARGIN:
            return {
                ...state,
                lowMargin: action.payload.results,
            };
            case actionTypes.GET_FLUCTUATE_MARGIN:
            return {
                ...state,
                fluctuate: action.payload.results,
            };
            case actionTypes.GET_RECIPE:
            return {
                ...state,
                recipes: action.payload.results,
            };
            case actionTypes.GET_INCORRECT_RECIPE:
            return {
                ...state,
                incorrectRecipe: action.payload.results,
            };
            case actionTypes.GET_DISABLED_RECIPE:
            return {
                ...state,
                disabledRecipe: action.payload.results,
            };
            case actionTypes.GET_UNTAGGED_RECIPE:
            return {
                ...state,
                untaggedRecipe: action.payload.results,
            };
            default:
                return state;
    }
}

export default reducer;