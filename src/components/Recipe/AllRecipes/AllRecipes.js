import React, { Suspense, useEffect, useState } from 'react';
import { Table, Form, FormCheck } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import '../recipeStyle.css';
import actions from '../../../actions/Actions';

const LazyloadRecipes = React.lazy(() => import('./LazyloadReciepes'));

const Recipes = (props) => {
    const [page, setPage] = useState(1);
    const [isFetching, setIsFetching] = useState(false);
    const [results, setResults] = useState([]);
    const [checkAll, setCheckAll] = useState(false);

    useEffect(async () => {
        await props.getAllRecipe(page);
    }, []);

    useEffect(() => {
        setResults(() => {
            return [...results, ...props.recipes]
        })
        window.addEventListener('scroll', handleScroll);
    }, [props.recipes]);

    const handleScroll = () => {
        if (
			Math.ceil(window.innerHeight + document.documentElement.scrollTop) !== document.documentElement.offsetHeight
		)
		return;
		setIsFetching(true);
    };

    useEffect(() => {
        setPage(page+1);
		if (!isFetching) return;
        if (page < 3) {
            fetchMoreRecipes();
        }
	}, [isFetching]);

	const fetchMoreRecipes = async () => {
		await props.getAllRecipe(page)
		setIsFetching(false);
	};

    const handleCheckAll = () => {
        setCheckAll(!checkAll);
    }

    const recipes = results.map((data,i) => (
        <Suspense key={data.id} fallback={<tr><td>Loading</td></tr>}>
            <LazyloadRecipes index={i} data={data} checkBox={checkAll} />
        </Suspense>
    ));

    return (
        <React.Fragment>
            <Table striped bordered hover>
            <thead>
                <tr>
                    <th>
                        <Form >
                            <FormCheck onChange={handleCheckAll} />
                        </Form>
                    </th>
                    <th>NAME</th>
                    <th>LAST UPDATED</th>
                    <th>COGS%</th>
                    <th>COST PRIC</th>
                    <th>SALE PRICE</th>
                    <th>GROSS MARGIN</th>
                    <th>TAGS / ACTIONS</th>
                </tr>
            </thead>
            <tbody>
                {recipes}
            </tbody>
            </Table>
        </React.Fragment>
    );
}

const mapStateToProps = (state) => ({
    recipes: state.recipes,
});

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            getAllRecipe: actions.getAllRecipe,
        },
        dispatch,
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(Recipes);