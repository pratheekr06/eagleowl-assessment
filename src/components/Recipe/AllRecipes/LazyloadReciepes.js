import React from 'react';
import { Form, FormCheck } from 'react-bootstrap';

const LazyloadRecipes = (props) => {
    
    return ( 
        <tr key={props.data.id}>
            <td>
            <Form>
                <FormCheck id={props.data.id} checked={props.checkBox || ''} />
            </Form>
            </td>
            <td>{props.data.name}</td>
            <td>{new Date(props.data.last_updated.date).toDateString().substring(4,10)}</td>
            <td>{props.data.cogs}%</td>
            <td>{Math.round(props.data.cost_price)}</td>
            <td>{Math.round(props.data.sale_price)}</td>
            <td>{Math.round(props.data.gross_margin)}</td>
            <td>
                <span className="tags p-1 mr-2">Tag</span>
                <span className="actions p-1">Action</span>
            </td>
        </tr>
     );
}
 
export default LazyloadRecipes;