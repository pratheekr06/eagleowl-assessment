import React, { useEffect, useState } from 'react';
import { Table, Form, FormCheck } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import './recipeStyle.css';
import actions from '../../actions/Actions';

const IncorrectRecipes = (props) => {
    const [checkAll, setCheckAll] = useState(false);

    useEffect(async () => {
        await props.getIncorrectRecipe();
    }, []);

    const handleCheckAll = () => {
        setCheckAll(!checkAll);
    }

    const incorrectRecipe = props.incorrectRecipe.map(data => {
        return (
            <tr key={data.id}>
                <td>
                <Form>
                    <FormCheck checked={checkAll || false} />
                </Form>
                </td>
                <td>{data.name}</td>
                <td>{new Date(data.last_updated.date).toDateString().substring(4,10)}</td>
                <td>{data.cogs}%</td>
                <td>{Math.round(data.cost_price)}</td>
                <td>{Math.round(data.sale_price)}</td>
                <td>{Math.round(data.gross_margin)}</td>
                <td>
                    <span className="tags p-1 mr-2">Tag</span>
                    <span className="actions p-1">Action</span>
                </td>
            </tr>
        )
    });

    return (
        <React.Fragment>
            {props.incorrectRecipe.length > 0 ? (
                <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>
                            <Form>
                                <FormCheck onChange={handleCheckAll} />
                            </Form>
                        </th>
                        <th>NAME</th>
                        <th>LAST UPDATED</th>
                        <th>COGS%</th>
                        <th>COST PRIC</th>
                        <th>SALE PRICE</th>
                        <th>GROSS MARGIN</th>
                        <th>TAGS / ACTIONS</th>
                    </tr>
                </thead>
                <tbody>
                   {incorrectRecipe}
                </tbody>
                </Table>
            ) : (
                <h2 className="text-center mt-3">No Data Found</h2>
            )}
        </React.Fragment>
    );
}

const mapStateToProps = (state) => ({
    incorrectRecipe: state.incorrectRecipe,
});

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            getIncorrectRecipe: actions.getIncorrectRecipe,
        },
        dispatch,
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(IncorrectRecipes);