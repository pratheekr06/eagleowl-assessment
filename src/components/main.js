import React, { Component } from 'react';
import { Row, Col, Container, Tabs, Tab } from 'react-bootstrap';
import './style.css';
import HighMargin from '../components/Margin/HighMargin';
import LowMargin from '../components/Margin/LowMargin';
import Fluctuate from '../components/Margin/Fluctuate';
import Recipes from '../components/Recipe/AllRecipes/AllRecipes';
import DisabledRecipes from '../components/Recipe/Disabled';
import IncorrectRecipes from '../components/Recipe/Incorrect';
import UntaggedRecipes from '../components/Recipe/Untagged';


class Main extends Component {
    render() {
        return (
            <React.Fragment>
                <Container className="my-4">
                    <Row>
                        <Col>
                            <HighMargin />
                        </Col>
                        <Col>
                            <LowMargin />
                        </Col>
                        <Col>
                            <Fluctuate />
                        </Col>
                    </Row>
                </Container>
                <Container>
                    <Tabs>
                        <Tab eventKey="recipes" title="ALL RECIPE(S)">
                            <Recipes />
                        </Tab>
                        <Tab eventKey="incorrect" title="INCORRECT">
                            <IncorrectRecipes />
                        </Tab>
                        <Tab eventKey="untagged" title="UNTAGGED">
                            <UntaggedRecipes />
                        </Tab>
                        <Tab eventKey="disabled" title="DISABLED">
                            <DisabledRecipes />
                        </Tab>
                    </Tabs>
                </Container>
            </React.Fragment>
        );
    }
}


export default Main;