import React, { useEffect} from 'react';
import { Table, Card } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import './marginStyle.css';
import actions from '../../actions/Actions';

const HighMargin = (props) => {

    useEffect(async () => {
        await props.getHighMargin();
    }, []);

    const highMarginRecipe = props.highMargin.map((data, i) => {
        return (
            <td key={i}>
                <p className="mb-0">{data.name}</p>
            </td>
        )
    });

    const marginIndicator = props.highMargin.map((data,i) => {
        return (
            <td key={i}>
                <CircularProgressbar value={data.margin} text={`${data.margin}%`} styles={buildStyles({pathColor: 'green'})} />
            </td>
        )
    })

    return (
        <React.Fragment>
            <Card>
                <Card.Header>High Margin Recipes</Card.Header>
                <Card.Body className="p-1">
                    <Table>
                        <tbody>
                            <tr className="tableRow">
                               {highMarginRecipe}
                            </tr>
                            <tr>
                                {marginIndicator}
                            </tr>
                        </tbody>
                    </Table>
                </Card.Body>
            </Card>
        </React.Fragment>
    );
}

const mapStateToProps = (state) => ({
    highMargin: state.highMargin,
});

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            getHighMargin: actions.getHighMargin,
        },
        dispatch,
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(HighMargin);