import React, { useEffect} from 'react';
import { Table, Card } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import './marginStyle.css';
import actions from '../../actions/Actions';

const LowMargin = (props) => {

    useEffect(async () => {
        await props.getLowMargin();
    }, []);

    const lowMarginRecipe = props.lowMargin.map((data, i) => {
        return (
            <td key={i}>
                <p className="mb-0">{data.name}</p>
            </td>
        )
    });

    const marginIndicator = props.lowMargin.map((data, i) => {
        return (
            <td key={i}>
                <CircularProgressbar value={data.margin} text={`${data.margin}%`} styles={buildStyles({pathColor: 'red'})} />
            </td>
        )
    })

    return (
        <React.Fragment>
            <Card>
                <Card.Header>Low Margin Recipes</Card.Header>
                <Card.Body className="p-1">
                    <Table>
                        <tbody>
                            <tr className="tableRow">
                               {lowMarginRecipe}
                            </tr>
                            <tr>
                                {marginIndicator}
                            </tr>
                        </tbody>
                    </Table>
                </Card.Body>
            </Card>
        </React.Fragment>
    );
}

const mapStateToProps = (state) => ({
    lowMargin: state.lowMargin,
});

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            getLowMargin: actions.getLowMargin,
        },
        dispatch,
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(LowMargin);