import React, { useEffect} from 'react';
import { Table, Card } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ArrowUp } from 'react-bootstrap-icons';
import './marginStyle.css';
import actions from '../../actions/Actions';

const Fluctuate = (props) => {

    useEffect(async () => {
        await props.getFluctuateMargin();
    }, []);

    const fluctuateRecipe = props.fluctuate.map((data, i) => {
        return <td key={i}>{data.name}</td>
    });

    const marginIndicator = props.fluctuate.map((data, i) => {
        return (
            <td key={i} className="indicator">
               {data.fluctuation}% <ArrowUp color="darkgreen" />
            </td>
        )
    });

    return (
        <React.Fragment>
            <Card>
                <Card.Header>Top FluctuateRecipe Recipes</Card.Header>
                <Card.Body className="p-1">
                    <Table>
                        <tbody>
                            <tr className="tableRow">
                               {fluctuateRecipe}
                            </tr>
                            <tr>
                                {marginIndicator}
                            </tr>
                        </tbody>
                    </Table>
                </Card.Body>
            </Card>
        </React.Fragment>
    );
}

const mapStateToProps = (state) => ({
    fluctuate: state.fluctuate,
});

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            getFluctuateMargin: actions.getFluctuateMargin,
        },
        dispatch,
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(Fluctuate);